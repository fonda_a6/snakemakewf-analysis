1. follow instructions from "https://carolinapb.github.io/2021-06-23-how-to-run-my-pipelines/"

2. after creating the conda environment there are more dependencies to install
    Importantly: the config.yaml file has to be edited with paths to data
    But I have no data to try this on

3. I suspend reproduction fo this workflow pending a resolution of the input data problem
    But the github page describes the workflow and the notebook interaction point in good detail:
        https://github.com/CarolinaPB/single-cell-data-processing
    Well, not that much detail. The interaction described seems to be on a run level:
        run the entire workflow once,
        look at the plots (on filesystem or using the notebooks)
        depending on those results change workflow thresholds in config file