number of repos: 1431
total includes: 3550
total modules: 415
resolved includes: 3436
number of repos with subworkflows: 10
number of subworkflow insertions: 20
number of repos with configfiles: 530
number of configfile insertions: 578
number of repos with inputfunc: 42
number of inputfunc insertions: 68
number of repos with checkpoint: 94
number of checkpoint insertions: 251
number of snakefiles: 1705
number of snakefiles with configfiles: 540
number of configs in snakefiles: 564
number of workflow files: 4850
number of files with out of rule controlflow: 560
number of files with leftmost controlflow: 560
