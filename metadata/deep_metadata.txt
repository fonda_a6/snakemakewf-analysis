num_repos: 1315
num_hunks: 158484
avg_hunk_per_repo: 120.52015209125476
hunk_count_25_percentile: 24
hunk_count_50_percentile: 60
hunk_count_75_percentile: 130
num_files: 48986
num_files_per_repo: 37.25171102661597
num_files_with_include: 928
num_files_with_module: 47
avg_hunk_add: 25.958860200398778
avg_hunk_del: 7.339081547664118
num_snakefiles: 2146
num_snakefile_hunks: 40092
num_snakefiles_with_include: 736
num_snakefiles_with_module: 41
avg_snakefile_hunk_add: 11.397884864810935
avg_snakefile_hunk_del: 5.03766337423925
avg_hunk_per_snakefile: 18.68219944082013
snakefile_hunk_count_25_percentile: 1
snakefile_hunk_count_50_percentile: 5
snakefile_hunk_count_75_percentile: 19
num_workflowfiles: 3642
num_workflowfile_hunks: 47466
num_workflowfiles_with_include: 742
num_workflowfiles_with_module: 42
avg_workflowfile_hunk_add: 11.882905658787342
avg_workflowfile_hunk_del: 4.881725866936334
avg_hunk_per_workflowfile: 13.032948929159803
workflowfile_hunk_count_25_percentile: 1
workflowfile_hunk_count_50_percentile: 3
workflowfile_hunk_count_75_percentile: 12
naive one_line hunk contextualisation matched targets: 6527
naive one_line hunk contextualisation missed targets: 5174
number of one line hunks: 11701
number of input one line hunks: 2052
number of output one linke hunks: 1577
number of run one linke hunks: 130
number of shell one linke hunks: 1430
number of params one linke hunks: 638
number of wrapper one linke hunks: 375
number of rule_head one linke hunks: 522
number of unclear one linke hunks: 4977
