1dayac/Novel-X

AAFC-BICoE/snakemake-mothur

AAFC-BICoE/snakemake-partial-genome-pipeline

AlejandroAb/CASCABEL

AlexanderLabWHOI/tara-euk-metaG

AlthafSinghawansaUHN/cfmedipseq_pipeline

Annaklumos/Hackathon

AntonieV/NodeRAD

AntonieV/fprdg1

AnzeLovse/mag

BDI-pathogens/ShiverCovid

BRITE-REU/snakemake-workshop

BiancaStoecker/complex-similarity-evaluation

Biochemistry1-FFM/uORF-Tools

BleekerLab/freebayes_snp_calling

BleekerLab/small-rna-seq-pipeline

CVUA-RRW/FooDMe

CarolinaPB/calculate-gene-abundance

CarolinaPB/gbs-genetic-fitness

CarolinaPB/nanopore-assembly

CarolinaPB/single-cell-data-processing

ChandanPavuluri/Snakemake_RNAseq_Preprocess_pipeline

CollinJ0/Snakemake-presto

DKFZ-ODCF/reference-data

DRMCRL/MFM-223_DHT-RNASeq

DarwinAwardWinner/CD4-csaw

DimitriMeistermann/rnaseq_align

Ducklegs17/intro_to_snakemake

EBISPOT/gwas-sumstats-harmoniser

ENCCS/word-count-hpda

ESR-NZ/human_genomics_pipeline

ESR-NZ/vcf_annotation_pipeline

EichlerLab/ONT_pipelines

EmilySNichols/nnunet-fetalbrain

FabianHofmann/pypsa-costallocation

FrancescoFerrari88/ams_umc

GabrielHoffman/muscat-comparison_v2

GediminasA/st16Sseqanalysis

HI-FRIENDS-SDC2/hi-friends

HelenaLC/muscat-comparison

HelenaLC/simulation-comparison

HenningTimm/bpht_evaluation_workflow

Hoohm/dropSeqPipe

IKIM-Essen/uncovar

IMS-Bio2Core-Facility/BIC086

JamieCFreeman/DGN_compatible

JetBrains-Research/chipseq-smk-pipeline

JoeBlackSci/SAHDA

JoelAAs/pgx_module

KoesGroup/Snakemake_ChIPseq_PE

Konjkov/QMC-snakemake-workflow

LUMC/HAMLET

LUMC/KeyGenes-dataprocessor

Leoberium/pyIPSA

LittleFool/snakemake-workflow-gsea

LorenaDerezanin/pipeline_test

MGXlab/benchmark_assembly

MUMC-MEDMIC/MACOVID

MW55/Natrix

MaevaTecher/varroa-host-jump

MariusCausemann/intracranialPulsation

MerrimanLab/variant_calling_pipeline

MetaSUB-CAMP/camp_short-read-taxonomy

Mira0507/snakemake_star

Monia234/LocalAncestry

Monia234/LocalAncestry_RFmix_admixture

Monia234/NCI-GEMSCAN

Monia234/admix-map

Monia234/admixMap

NCBI-Hackathons/Kipoi-GWAS

NCI-CGR/GEMSCAN

NIH-HPC/snakemake-class

NKI-CCB/imagene-analysis

NovembreLab/eems-around-the-world

ORU-BIOINF/metagenomics-workflow

Ong-Research/microbiome-pipeline

Ovewh/Climaso

PEDIA-Charite/PEDIA-workflow

PEDIA-Charite/classifier

PNNL-CompBio/perseq

PerinatalLab/metaGWAS

Phil9S/swgs-gistic

Pinjontall94/asd-q2

PyPSA/pypsa-eur

PyPSA/pypsa-eur-mga

PyPSA/pypsa-eur-sec

PyPSA/pypsa-za

RBL-NCI/iCLIP

RawalTeam/MeX-Pipeline

RickGelhausen/HRIBO

RickGelhausen/RiboReport

SansamLab/Cut_And_Run_Analysis_SnakeMake

SchlossLab/Sovacool_OptiFit_mSphere_2022

SherineAwad/BulkRNASeq

SherineAwad/SomaticMutations

SherineAwad/StructureVariations

SilasK/16S-dada2

SilkeAllmannLab/pacbio_snakemake

Swart-lab/karyocode-workflow

SycuroLab/metqc

TBradley27/FilTar

ThermofisherAndBabraham/polyAterminus

ThomasBrazier/LDRecombinationMaps-pipeline

TriLab-bioinf/RNAseq_bw_smk_guydosh

Ulthran/ShotgunUnifrac

VUEG/bdes_to

ZeweiSong/metaSeq

acshetty/sncRNA-seq-analysis

aehrc/TRIBES

akcorut/SnakeRNASeq

akhanf/diffparc-smk

akhanf/greedy_template_camcan

akhanf/greedy_template_marm

akhanf/nnunet-fetalbrain

akhanf/zona-diffparc

alephreish/uniclust-snakemake

alexmsalmeida/metamap

alexmsalmeida/virsearch

algbio/panvc-founders

algbio/practical-omnitigs

alipirani88/samosa

amberr098/snakemakeOWE11

andersonbrito/ncov

andnischneider/its_workflow

aomlomics/tourmaline

aponsero/Viral_hunt_snakemake

argschwind/TAPseq_manuscript

arm61/thesis

arthurvinx/MEDUSA

arthurvinx/Medusa

aryarm/as_analysis

bayraktar1/RPCA

bcgsc/pori_graphkb_loader

bhattlab/StrainSifter

bhattlab/lathe

bhklab/MERIDA_snakemake_pipeline

bihealth/varfish-db-downloader

billbrod/foveated-metamers

billbrod/spatial-frequency-preferences

bioconda/bioconda-paper

biocore/oecophylla

bitextor/bitextor-neural

blab/norovirus

boasvdp/MRA_Streptococcus_suis

bposzewiecka/PhaseDancerSimulator

bposzewiecka/tytus

brynpickering/wind-spores

calliope-project/euro-calliope

calliope-project/sector-coupled-euro-calliope

carbonelab/hic_workflow

carlacummins/cv19-lineages-workflow

cauliyang/tools-compare

cbp44/snakemake-workflow-ref_genomes

ccerutti88/circDetector

centuri-engineering/Microbiome-Analysis-Workflow

clinical-genomics-uppsala/accel_amplicon_trimming

cmdoret/Acastellanii_genome_analysis

cmdoret/Acastellanii_hybrid_assembly

cnio-bu/cluster_rnaseq

cnio-bu/protter

cnio-bu/varca

cnobles/iGUIDE

cpauvert/PLNnetwork-workflow

cpauvert/smk-wk-as-module

crazyhottommy/Genrich_compare

crazyhottommy/pyflow-ChIPseq

csbl-inovausp/snakemake_lab_training

csoeder/PopPsiSeq

csoeder/VolkanLab_BehaviorGenetics

ctmrbio/stag-mwc

dannyxbergeron/Custom_annotation_Scottlab_2020

datasnakes/orthoevol-snakemake

davemcg/OGVFB_RNAseq

david-a-parry/perturbseq_workflow

davidries84/bismark_snakemake

deanpettinga/ecc_workflow

dence/rnastar_deseq2_MeJA_timecourse

dence/rnastar_deseq2_isoseq_trial

dence/rnastar_dseq2_old_RNAseq

dib-lab/2018-ncbi-lineages

dib-lab/2021-metapangenome-example

dib-lab/2021-orpheum-sim

dieterich-lab/single-cell-nanopore

dirkrepsilber/metagenomics-workflow

docmanny/smRecSearch

dossgollin-lab/nexrad-xarray

dpmerrell/TrialMDP-analyses

dpmerrell/tcga-pipeline

dredge-bio/dredge-sc-snakemake

eanbit-rt/Workflows_and_package_management

ebi-gene-expression-group/bulk-recalculations

ebi-gene-expression-group/cross-species-cellgroup-comparison

ebi-gene-expression-group/wf-bulk-indexing

ed-lau/riana

ejongepier/NanoClass

ekirving/refpanel

elimoss/lathe

emleddin/md-analysis-workflow

eriqande/phase-chromosomes-snakeflow

etiennefc/t_thermophila_RNA_Seq

euronion/snakemake-demo

felixleopoldo/benchpress

fieldima/biof501-pipeline

fischer-hub/metagenomics

franciscozorrilla/metaGEM

frankier/vocabaqdata

friendsofstrandseq/ashleys-qc-pipeline

ftabaro/MethylSnake

gagneurlab/ALS-kaggle

genomewalker/aMGSIM-smk

genxnetwork/grape

gitter-lab/ssps

gosling-lang/gos-example

groverj3/wgbs_snakemake

gustaveroussy/rna-count-kallisto

hivlab/discover-virome

hobrien/DiezmannLabManduca

hydra-genetics/misc

hydra-genetics/prealignment

hzi-bifo/snakemake-workflow-univec-removal

ilsenatorov/rindti

iqbal-lab-org/viridian_simulations_workflow

iwohlers/2020_mt_analyses

iwohlers/lied_egypt_genome

j23414/test_snakemake_6

j2moreno/biobakery

jaleezyy/covid-19-signal

jaredslosberg/get_SRA_fastqs

jbloomlab/Ab-CGGnaive_DMS

jbloomlab/IFNsorted_flu_single_cell

jbloomlab/SARS-CoV-2-RBD_B.1.351

jbloomlab/SARS-CoV-2-RBD_DMS

jbloomlab/SARS-CoV-2-RBD_DMS_variants

jbloomlab/SARS-CoV-2-RBD_Delta

jbloomlab/SARS-CoV-2-RBD_MAP_Crowe_antibodies

jbloomlab/SARS-CoV-2-RBD_MAP_OAS

jbloomlab/SARS-CoV-2-RBD_MAP_clinical_Abs

jbloomlab/SARSr-CoV_RBD_MAP

jbloomlab/SARSr-CoV_homolog_survey

jdossgollin/2021-TXtreme

jeongdo801/scNOVA

jerry955071/library_annotation

jhamman/chains

jhawe/bggm

jiarong/SSUsearch

jiarong/xander-assembly-pipeline

jlanga/smsk_454

jlanga/smsk_gatk_germline

jlanga/smsk_popoolation

jlanga/smsk_trinotate

jlanga/smsk_tuxedo2

jmeppley/np_read_clustering

jrderuiter/snakemake-rnaseq

jwdebelius/avengers-assemble

kaiseriskera/FABLE

kcivkulis/stag-mwc

kelly-sovacool/asm-microbe-2022

kelly-sovacool/test-opti-random

khalillab/coop-TF-custom-analyses

khalillab/coop-TF-rnaseq

khanlab/greedy_template_neonatal

khembach/TDP-CLIPseq

khusmann/eclsk-mmod

kircherlab/CADD-SV

kircherlab/CADD-scripts

kircherlab/MPRAsnakeflow

kircherlab/ReMM

kircherlab/hemoMIPs

koesterlab/chm-synthetic-tumor-normal

koesterlab/nanopore-qc

komparo/rosetta-pipeline

konnorve/Variant-finding-workflow

lachlandeer/angrist-krueger-1991

lachlandeer/snakemake-econ-r

langmead-lab/recount-unify

larapozza/Snakemake-Tutorial

lculibrk/Ploidetect-pipeline

leipzig/placenta

leoisl/pandora_workflow

leomorelli/scGET

leylabmpi/Struo

lisakmalins/Snakespeare

lorenziha/RNAseq_biowulf_smk

lparsons/kas-seq-workflow

lrgr/sigma

macmanes-lab/DoveParentsRNAseq

mal2017/TestisTEs2021

manaakiwhenua/virtual-landscape-ecosys-services

manzt/zhou-et-al-natgen-2020

marcjwilliams1/dnds-clonesize

mardzix/bcd_nano_CUTnTag

marykthompson/ribopop_probe_design

marykthompson/ribopop_rnaseq

matrs/16s-rRNA-Sanger

matrs/Quality_control

matt-sd-watson/annotate_ncov_trees

maxsonBraunLab/Bulk-RNA-seq-pipeline-SE

meringlab/og_consistency_pipeline

metagenome-atlas/genecatalog_atlas

metamaden/recountmethylation_instance

meyer-lab-cshl/rnaseq-tutorial

mfansler/mm10-db

mgildea87/CUT-RUN

mgildea87/ChIPseq

mgildea87/RNAseq_PE

micom-dev/paper

mike-molnar/nanopore-workflow

mikegloudemans/post-coloc-toolkit

mikelperez01/Mike

mishaploid/Bo-demography

mishaploid/carrot-demography

mitoNGS/MToolBox_snakemake

momo54/sage-orderby-experiment

monolo90/Gatk_snakemake

mozilla/firefox-translations-training

mrvollger/StainedGlass

mrvollger/sd-divergence

murrayds/covid19-dimensions-analysis

murrayds/sci-text-disagreement

myonaung/sm-SNIPER

nandsra21/NLPProject

nanoporetech/DTR-phage-pipeline

nanoporetech/Pore-C-Snakemake

nanoporetech/pipeline-transcriptome-de

nasiegel88/10x-snake

nathanhaigh/ronin_snakemake_demo

nbren12/uwnet

nchris5/diffparc-smk_piriform

neherlab/gisaid_nextstrain

neon-ninja/crash_prediction

nextstrain/monkeypox

ngs-docs/2020-ggg-201b-rnaseq

ngs-docs/2022-rnaseq-cloud

nih-cfde/update-content-registry

nioo-knaw/PhyloFunDB

nsteinau/kallisto-snakemake

ohsu-cedar-comp-hub/cfRNA-seq-pipeline-Ngo-manuscript-2019

oma219/khoice

open2c/inspectro

open2c/quaich

opentargets/evidence_datasource_parsers

osvaldoreisss/polya-seq_workflow_analysis

pachterlab/PROBer_paper_analysis

pd321/chipseq

pha4ge/hAMRonization_workflow

plycrsk/rna-seq-star-deseq2

pmalonis/lfads_analysis

pmenzel/ont-assembly-snake

pmenzel/score-assemblies

pughlab/PLBR_MedRemixBEDPE

pughlab/cfMeDIP-seq-analysis-pipeline

pypsa-meets-africa/pypsa-earth-sec

qbicsoftware-archive/rnaseq

qferre/ologram-modl_supp_mat

ramay/dada2_snakemake_workflow

ravenclown/Fusarium_workflow

related-sciences/ukb-gwas-pipeline-nealelab

rikenbit/scTensor-experiments

rimjhimroy/SnakeGATK

robinmeyers/cutnrun-snakemake

robinmeyers/mpra-gwas-builder

roshnipatel/LocalAncestry

rwaples/lai-sim

ryanpeek/r_with_snakemake

sabiqali/strline

sanjaynagi/probe

santonow/masters-thesis

scottgigante/haplotyped-methylome

scottgroup/Stringent_RNA_seq

scottgroup/permutations-ica

scottgroup/yeast-ribosomal-protein-paralogs

seb-mueller/snakemake-bisulfite

sevyharris/autoscience_workflow

sevyharris/qe_workflow

simakro/smk-artic-ont

sinanugur/sncRNA-workflow

sinanugur/spatial-workflow

singleron-RD/Dna-seq_Analysis

skurscheid/camda2019-workflows

skurscheid/cellcycle_workflow

skurscheid/mcf10_promoter_profiling

skurscheid/snakemake

snakemake-workflows/dna-seq-benchmark

snakemake-workflows/ngs-test-data

snakemake-workflows/rna-seq-kallisto-sleuth

snakemake-workflows/single-cell-rna-seq

sneuensc/mapache

snystrom/cutNrun-pipeline

solida-core/musta

spjuhel/BoARIO

sraorao/snakemake_code_clinic_2

sschmutz/thesis-workflow

steveped/snakemake_hic

stracquadaniolab/workflow-pygna

supermaxiste/ARPEGGIO

svsuresh/salmon_deseq2_snakemake

sylvainschmitt/detectMutations

sylvainschmitt/generateMutations

sylvainschmitt/genomeAnnotation

sylvainschmitt/transcriptoAechmea

tallgran/conifer-long-read-polishing

tallgran/conifer-short-read-polishing

tanaes/snake_strainer

tanaes/snakemake_assemble

tavareshugo/publication_Tavares2022_Nselection

taylorreiter/2021-cami-annot

taylorreiter/2022-infant-mge

taylorreiter/olive_genome

tgac-vumc/QDNAseq.snakemake

tgstoecker/MuWU

theislab/scib-pipeline

thomasbtf/document-anonymization

timoast/MPAL-hg38

timoast/basc

timoast/epi-immune

timoast/nanobody

timoast/signac-paper

timplab/ambic-epigenome

timtroendle/geographic-scale

timtroendle/money-land

timtroendle/possibility-for-electricity-autarky

tjbencomo/bulk-rnaseq

tjbencomo/ngs-pipeline

tlestang/ntdmc_snakemake_pipeline

tpletzer/wrfh_postprocess_workflow

trajkovski-lab/Quality-filtering

trofimovamw/GInCovPipe

usnistgov/giab-asm-benchmarking

vari-bbc/Peaks_workflow

vari-bbc/prep_bbc_shared

vari-bbc/rnaseq_workflow

vari-bbc/scRNAseq

varlociraptor/varlociraptor-evaluation

vibaotram/baseDmux

waglecn/mabs

williamjeong2/snakemake_DNA-seq

williamjeong2/snakemake_RNA-seq

xiangyugits/rnaseq-salmon-deseq2

xiangyugits/wgs

yanhui-k/Natrix

yanhui09/RGIcat

yzeng-lol/tcge-cfmedip-seq-pipeline

yztxwd/rna-seq-standard-pipeline

yztxwd/snakemake-pipeline-general

zavolanlab/bindz-rbp

zifornd/memeth

