0., repo: genxnetwork/grape, _id: 630d04299cfec0ee85ec00a1:
-rule split_map:
-    output: expand("cm/chr{chrom}.cm.map", chrom=CHROMOSOMES)
-        cm_dir='cm'
-        cm=expand("cm/chr{chrom}.cm.map", chrom=CHROMOSOMES)
-        cm_dir='cm',
+rule split_map_for_ibd:
+    output: expand("cm2/chr{chrom}.cm.map", chrom=CHROMOSOMES)
+        cm_dir='cm2'
+        cm=expand("cm2/chr{chrom}.cm.map", chrom=CHROMOSOMES)
+        cm_dir='cm2',
-------------------------------------------------------------------------
1., repo: bioconda/bioconda-paper, _id: 630e8b760ff667b0c903e083:
-rule authors:
-        
+rule author_list:
+        "resources/authors.tsv"
+    output:
+        "resources/authors.tex"
+    conda:
+        "envs/analysis.yaml"
+    script:
+        "scripts/author-list.py"
-------------------------------------------------------------------------
2., repo: groverj3/wgbs_snakemake, _id: 630e8c140ff667b0c903e0cc:
-rule fastqc_cat:
-# Trim the concatenated files
-        # Get bounds for inclusion
+rule fastqc_raw:
+# Trim the read pairs
+        # Get bounds for inclusion from the .mbias files
-------------------------------------------------------------------------
3., repo: cpauvert/smk-wk-as-module, _id: 630ebd490ff667b0c903e43f:
-rule convert_fastq:
-        "vsearch --fastq_convert {input} --fasta_out {output}"
+rule unique_sequences:
+        "vsearch --fastx_uniques {input} --fastaout {output} --fasta_width 0 --threads {threads}"
-------------------------------------------------------------------------
4., repo: leylabmpi/Struo, _id: 630f049a0ff667b0c903e706:
-rule braken_build:
-        mem_gb_pt = lambda wildcards, attempt: attempt ** 2
+rule bracken_build:
+        mem_gb_pt = lambda wildcards, attempt: (attempt + 1) ** 3
+        # location of the kraken2 db files
+        # removing existing files possibly created by bracken
+        TMP_FILE=$DB"/database.kraken"
+        rm -f $TMP_FILE
-------------------------------------------------------------------------
5., repo: leylabmpi/Struo, _id: 630f049a0ff667b0c903e712:
-rule braken_build:
-        mem_gb_pt = lambda wildcards, attempt: attempt ** 2
+rule bracken_build:
+        mem_gb_pt = lambda wildcards, attempt: (attempt + 1) ** 3
+        # location of the kraken2 db files
+        # removing existing files possibly created by bracken
+        TMP_FILE=$DB"/database.kraken"
+        rm -f $TMP_FILE
-------------------------------------------------------------------------
6., repo: leylabmpi/Struo, _id: 630f049a0ff667b0c903e71c:
-rule braken_build:
-        mem_gb_pt = lambda wildcards, attempt: attempt ** 2
+rule bracken_build:
+        mem_gb_pt = lambda wildcards, attempt: (attempt + 1) ** 3
+        # location of the kraken2 db files
+        # removing existing files possibly created by bracken
+        TMP_FILE=$DB"/database.kraken"
+        rm -f $TMP_FILE
-------------------------------------------------------------------------
7., repo: sevyharris/autoscience_workflow, _id: 630f38280ff667b0c903eab4:
-rule calculate_species_thermo:
-        echo $DFT_DIR
+rule species_thermo:
+        bash -c '
+            . $HOME/.bashrc
+            conda activate /work/westgroup/harris.se/tst_env
+            python scripts/species_thermo.py 4
+            #sbatch scripts/run_species_thermo.sh
+            conda deactivate'
-------------------------------------------------------------------------
8., repo: pachterlab/PROBer_paper_analysis, _id: 630f52630ff667b0c903ec4d:
-rule compile_mappability:
-		script_path + "/calcMappability.cpp"
-		script_path + "/calcMappability"
+rule compile_cpp:
+		"{path}/{{program}}.cpp".format(path = script_path)
+		"{path}/{{program,\w+}}".format(path = script_path)
-------------------------------------------------------------------------
9., repo: pachterlab/PROBer_paper_analysis, _id: 630f52630ff667b0c903ec65:
-rule compile_mappability:
-		script_path + "/calcMappability.cpp"
-		script_path + "/calcMappability"
+rule compile_cpp:
+		"{path}/{{program}}.cpp".format(path = script_path)
+		"{path}/{{program,\w+}}".format(path = script_path)
-------------------------------------------------------------------------
10., repo: zezzipa/Poirot_RD-WGS, _id: 630f6c7d0ff667b0c903edb2:
-rule all:
-    input:
-        "vcf_filter/NA24385.sort.vcf.gz"
-        #expand("vcf_filter/{sample}.sort.vcf.gz", sample=config["samples"]),
-
-#
-#        "/projects/wp3/nobackup/Workspace/WGS_pipeline_GPU_test/filter/NA12878.sort.vcf.gz",
-#        expand("filter/{sample}.sort.vcf.gz"),
+rule all:
+    input:
+        ["vcf_filter/%s.sort.vcf.gz" % sample for sample in get_samples(samples)]
+
+
-------------------------------------------------------------------------
11., repo: kelly-sovacool/test-opti-random, _id: 630f81e50ff667b0c903ede2:
-rule render_readme:
-    input:
-        rmd='README.Rmd'
-    output:
-        md="README.md"
-    shell:
-        """
-        R -e "rmarkdown::render('{input.rmd}')"
-        """
+rule render_readme:
+    input:
+        rmd='README.Rmd',
+        tsv46='results/optifit_1.46.1.tsv',
+        tsv47='results/optifit_split_results.tsv'
+    output:
+        md="README.md"
+    shell:
+        """
+        R -e "rmarkdown::render('{input.rmd}')"
+        """
+
-------------------------------------------------------------------------
12., repo: KoesGroup/Snakemake_ChIPseq_PE, _id: 630f9a3f0ff667b0c903ef09:
-rule bigwig:
-        EFFECTIVEGENOMESIZE = str(config["bamCoverage"]["params"]["EFFECTIVEGENOMESIZE"]), #take argument separated as a list separated with a space
-        EXTENDREADS         = str(config["bamCoverage"]["params"]["EXTENDREADS"])
-        "bamCoverage --bam {input} -o {output} --effectiveGenomeSize {params.EFFECTIVEGENOMESIZE} --extendReads {params.EXTENDREADS} &>{log}"
-        "bamCompare -b1 {input.treatment} -b2 {input.control} -o {output.bigwig} &>{log}"
-        plot   = str(config['plotHeatmap']['plot'])
-        --legendLocation best"
+rule bamCoverage:
+        EFFECTIVEGENOMESIZE     = str(config["bamCoverage"]["params"]["EFFECTIVEGENOMESIZE"]), #take argument separated as a list separated with a space
+        EXTENDREADS             = str(config["bamCoverage"]["params"]["EXTENDREADS"]),
+        binSize                 = str(config['bamCoverage']["params"]['binSize']),
+        normalizeUsing          = str(config['bamCoverage']["params"]['normalizeUsing']),
+        ignoreForNormalization  = str(config['bamCoverage']["params"]['ignoreForNormalization']),
+        smoothLength            = str(config['bamCoverage']["params"]['smoothLength'])
+        "bamCoverage --bam {input} \
+        -o {output} \
+        --effectiveGenomeSize {params.EFFECTIVEGENOMESIZE} \
+        --extendReads {params.EXTENDREADS} \
+        --binSize {params.binSize} \
+        --smoothLength {params.smoothLength} \
+        --ignoreForNormalization {params.ignoreForNormalization} \
+        &>{log}"
+    params:
+        binSize             = str(config['bamcompare']['binSize']),
+        normalizeUsing      = str(config['bamcompare']['normalizeUsing']),
+        EFFECTIVEGENOMESIZE = str(config["bamcompare"]["EFFECTIVEGENOMESIZE"]),
+        operation           = str(config['bamcompare']['operation']),
+        smoothLength        = str(config['bamcompare']['smoothLength']),
+        ignoreForNormalization = str(config['bamcompare']['ignoreForNormalization']),
+        scaleFactorsMethod  = str(config['bamcompare']['scaleFactorsMethod'])
+        "bamCompare -b1 {input.treatment} \
+        -b2 {input.control}  \
+        --binSize {params.binSize} \
+        -o {output.bigwig} \
+        --normalizeUsing {params.normalizeUsing} \
+        --operation {params.operation} \
+        --smoothLength {params.smoothLength} \
+        --ignoreForNormalization {params.ignoreForNormalization} \
+        --scaleFactorsMethod {params.scaleFactorsMethod} \
+        &>{log}"
+        plot   = str(config['plotHeatmap']['plot']),
+        cluster = "{treatment}_vs_{control}.bed"
+        --legendLocation best \
+        --outFileSortedRegions {params.cluster}"
-------------------------------------------------------------------------
13., repo: spjuhel/BoARIO, _id: 630f9a7c0ff667b0c903ef3a:
-rule run_Full_dmg:
-        flood="max|min|[1-99]%"
+rule run_Full_dmg_raw:
+        flood="max|min|\d{1,2}%"
-------------------------------------------------------------------------
14., repo: SchlossLab/Sovacool_OptiFit_mSphere_2022, _id: 630fcafe0ff667b0c903f13d:
-rule subtargets:
-        R='code/R/summarize_results.R'#,
-        # opticlust=prep_samples('results/opticlust_results.tsv'),
-        # optifit_db=fit_ref_db('results/optifit_dbs_results.tsv'),
-        # optifit_split=fit_split('results/optifit_split_results.tsv'),
-        # vsearch=vsearch('results/vsearch_results.tsv')
+rule subtargets: # it takes a long time to build the DAG for some of these
+        R='code/R/summarize_results.R',
+        opticlust='subworkflows/1_prep_samples/results/opticlust_results.tsv',
+        optifit_db='subworkflows/2_fit_reference_db/results/optifit_dbs_results.tsv',
+        optifit_split='subworkflows/3_fit_sample_split/results/optifit_split_results.tsv',
+        vsearch='subworkflows/4_vsearch/results/vsearch_results.tsv'
-------------------------------------------------------------------------
15., repo: RBL-NCI/iCLIP, _id: 630fcb3a0ff667b0c903f21d:
-rule sort_index_stats:
-        rname='06_sort_index_stats',
-    threads: getthreads("sort_index_stats")
-        # Sort and Index
-        samtools sort --threads {threads} -m 10G -T ${{tmp_dir}} \\
-            {input.bam} \\
-            -o {output.bam};
+rule index_stats:
+        rname='06_index_stats',
+    threads: getthreads("index_stats")
+        # Index
-------------------------------------------------------------------------
16., repo: manaakiwhenua/virtual-landscape-ecosys-services, _id: 630ffc430ff667b0c903f4ed:
-rule clean:
-    rm -rf "results/"
-
+rule clean:
+    shell:
+        rm -rf "results/"
+
-------------------------------------------------------------------------
17., repo: ccerutti88/circDetector, _id: 630ffc500ff667b0c903f4fd:
-rule mergemappingstat:
-        "python3 ../scripts/stats_mapping.py -i {input} -o {output}"
+rule mappingstat:
+        "python3 ../scripts/stats_mapping.py -i {input} -o {output}"
-------------------------------------------------------------------------
18., repo: cnio-bu/protter, _id: 630ffe850ff667b0c903f5cd:
-rule convert_leucines:
-        with open(input.db) as fh:
-            with open(output.db,"w") as ofh:
+rule process_db:
+        with open(output.db,"w") as ofh:
+            with open(input.db) as fh:
+            if config["dbs"][wildcards.db]["add_crap"]:
+                with open("res/data/seq/crap/crap.fasta") as fh:
+                    for r in parsefastx(fh):
+                        ofh.write(">{}\n{}\n".format(r[0],r[1].replace("L","I")))
-------------------------------------------------------------------------
19., repo: macmanes-lab/DoveParentsRNAseq, _id: 63102ee60ff667b0c903f7d7:
-rule fig234:
-    "figures/fig3-1.pdf",
-    "figures/fig4-1.pdf"
-    "Rscript analysis/05_figs234.R" 
+rule figs23:
+    "figures/fig3-1.pdf"
+    "Rscript analysis/05_figs23.R" 
-------------------------------------------------------------------------
20., repo: Mira0507/snakemake_sra, _id: 631045f50ff667b0c903f878:
-rule get_fastq:   # Creates fastq.gz files in fastq directory
-        shell("fastq-dump --split-files {sra} --gzip -X 100000")
+rule get_fastq:   
+        shell("fastq-dump --split-files {sra} --gzip -X 100000")   # -X is for testing
-------------------------------------------------------------------------
21., repo: argschwind/TAPseq_manuscript, _id: 63105f4d0ff667b0c903fa20:
-rule alignment_reference:
-    "data/genome_reference/cropseq_ref.dict",
-    "data/genome_reference/cropseq_ref.refFlat",
-    "data/genome_reference/genomeDir",
-    "data/tapseq_ref_validation/cropseq_ref.dict",
-    "data/tapseq_ref_validation/cropseq_ref.refFlat",
-    "data/tapseq_ref_validation/genomeDir",
-    "data/tapseq_ref_chr8_screen/cropseq_ref.dict",
-    "data/tapseq_ref_chr8_screen/cropseq_ref.refFlat",
-    "data/tapseq_ref_chr8_screen/genomeDir",
-    "data/tapseq_ref_chr11_screen/cropseq_ref.dict",
-    "data/tapseq_ref_chr11_screen/cropseq_ref.refFlat",
-    "data/tapseq_ref_chr11_screen/genomeDir",
-    "data/genome_reference_v2/cropseq_ref.dict",
-    "data/genome_reference_v2/cropseq_ref.refFlat",
-    "data/genome_reference_v2/genomeDir",
-    "data/tapseq_ref_validation_v2/cropseq_ref.dict",
-    "data/tapseq_ref_validation_v2/cropseq_ref.refFlat",
-    "data/tapseq_ref_validation_v2/genomeDir",
-    "data/tapseq_ref_validation_v3/cropseq_ref.dict",
-    "data/tapseq_ref_validation_v3/cropseq_ref.refFlat",
-    "data/tapseq_ref_validation_v3/genomeDir",
-    "data/genome_reference_dropseq/cropseq_ref.dict",
-    "data/genome_reference_dropseq/cropseq_ref.refFlat",
-    "data/genome_reference_dropseq/genomeDir",
-    "data/tapseq_ref_dropseq/cropseq_ref.dict",
-    "data/tapseq_ref_dropseq/cropseq_ref.refFlat",
-    "data/tapseq_ref_dropseq/genomeDir"
+rule alignment_references:
+    expand("data/alignment_references/{align_ref}/{ref_file}",
+      align_ref = ["hg38_genome_ref", "hg38_tapseq_ref_validation", "hg38_tapseq_ref_chr8_screen",
+        "hg38_tapseq_ref_chr11_screen", "hg38_genome_ref_v2", "hg38_tapseq_ref_validation_v2",
+        "hg38_tapseq_ref_validation_v3", "hg38_genome_ref_dropseq", "hg38_tapseq_ref_dropseq",
+        "mm10_genome_ref", "hg38_genome_ref_rev", "hg38_tapseq_ref_rev", "mm10_tapseq_ref_mix",
+        "hg38_tapseq_ref_l1000"],
+      ref_file = ["cropseq_ref.dict", "cropseq_ref.refFlat", "genomeDir"])
-------------------------------------------------------------------------
22., repo: jbloomlab/SARS-CoV-2-RBD_DMS_variants, _id: 631092450ff667b0c903fd31:
-    rule build_ccs:
-        """Run PacBio ``ccs`` program to build CCSs from subreads."""
-            subreads=lambda wildcards: (pacbio_runs
-                                        .at[wildcards.pacbioRun, 'subreads']
-            ccs_report=os.path.join(config['ccs_dir'], "{pacbioRun}_report.txt"),
-        params:
-            min_ccs_length=config['min_ccs_length'],
-            max_ccs_length=config['max_ccs_length'],
-            min_ccs_passes=config['min_ccs_passes'],
-            min_ccs_accuracy=config['min_ccs_accuracy']
-        threads: config['max_cpus']
-            """
-            ccs \
-                --min-length {params.min_ccs_length} \
-                --max-length {params.max_ccs_length} \
-                --min-passes {params.min_ccs_passes} \
-                --min-rq {params.min_ccs_accuracy} \
-                --report-file {output.ccs_report} \
-                --num-threads {threads} \
-                {input.subreads} \
-                {output.ccs_fastq}
-            """
+    rule get_ccs:
+        """Symbolically link CCS files."""
+            ccs_fastq=lambda wildcards: (pacbio_runs
+                                        .at[wildcards.pacbioRun, 'ccs']
+            "os.symlink({input.ccs_fastq}, {output.ccs_fastq})"
-------------------------------------------------------------------------
23., repo: akhanf/diffparc-smk, _id: 6310d8ec0ff667b0c9040052:
-rule all:
-subworkflow hcp_mmp_to_native:
-    workdir: '../subworkflows/hcp_mmp_to_native'
-    snakefile: '../subworkflows/hcp_mmp_to_native/workflow/Snakefile'
-    configfile: 'config/config.yml'
-
+rule all_diffparc:
+include: '../subworkflows/hcp_mmp_to_native/workflow/Snakefile'
-------------------------------------------------------------------------
24., repo: sansamcl/SnakemakeRepositoryTemplate, _id: 6310f2db0ff667b0c90402bf:
-rule bwa_mem2_mem:
-        "results/logs/bwa_mem2/{sample}.log",
-        sort="samtools",  # Can be 'none', 'samtools' or 'picard'.
-        sort_order="coordinate",  # Can be 'coordinate' (default) or 'queryname'.
-        "v1.3.2/bio/bwa-mem2/mem"
+rule bwa_mem:
+        "results/logs/bwa_mem/{sample}.log",
+    envmodules:
+         "bwa/0.7.15",
+         "samtools/1.14",
+         "picard/2.21.2",
+        sorting="samtools",  # Can be 'none', 'samtools' or 'picard'.
+        sort_order="coordinate",  # Can be 'queryname' or 'coordinate'.
+        "v1.3.2/bio/bwa/mem"
-------------------------------------------------------------------------
25., repo: nbren12/uwnet, _id: 63113b790ff667b0c9040506:
-rule nudge_run:
-    output: directory(f"data/runs/{TODAY}-nudging")
-    -t 0 -p assets/parameters_nudging.json {output}
+rule nudging_run:
+    # need to use a temporary file here so that the model output isn't deleted
+    output: touch(f"data/runs/{TODAY}-nudging/.done")
+    params: rundir=f"data/runs/{TODAY}-nudging",
+            ngaqua=DATA_PATH,
+            sam_src=sam_src,
+            sam_params="assets/parameters_nudge_nn.json",
+            step=0
+    rm -rf {params.rundir}
+    -n {params.ngaqua} \
+    -s {params.sam_src} \
+    -t {params.step} \
+    -p {params.sam_params} \
+    {params.rundir}
+    # run sam
+    cd {params.rundir}
+    sh run.sh
+
-------------------------------------------------------------------------
26., repo: LUMC/HAMLET, _id: 63113d350ff667b0c90405fb:
-rule all:
-        base_per_exon=[RUN.output(f) for f in expand("{sample}/expression/{sample}.bases_per_exon" ,sample=RUN.samples)]
-        #base_per_gene=expand("{sample}.bases_per_gene", sample=RUN.samples),
-        #exon_ratio=expand("{sample}.exon_ratios", sample=RUN.samples),
-        #fragment_per_gene=expand("{sample}.fragments_per_gene", sample=RUN.samples),
-        #raw_base=expand("{sample}.raw_base", sample=RUN.samples)
-        " | python {input.calc_script} -r {params.exon_min_ratio}"
+rule all_expression:
+        base_per_exon=[RUN.output(f) for f in expand("{sample}/expression/{sample}.bases_per_exon" ,sample=RUN.samples)],
+        base_per_gene=[RUN.output(f) for f in expand("{sample}/expression/{sample}.bases_per_gene", sample=RUN.samples)],
+        exon_ratio=[RUN.output(f) for f in expand("{sample}/expression/{sample}.exon_ratios", sample=RUN.samples)],
+        fragment_per_gene=[RUN.output(f) for f in expand("{sample}/expression/{sample}.fragments_per_gene", sample=RUN.samples)],
+        raw_base=[RUN.output(f) for f in expand("{sample}/expression/{sample}.raw_base", sample=RUN.samples)]
+        " | python3 {input.calc_script} -r {params.exon_min_ratio}"
-------------------------------------------------------------------------
27., repo: lisakmalins/Snakespeare, _id: 63113d550ff667b0c9040617:
-rule join_metrics:
-        "Rscript scripts/join_metrics.R {input} {output}"
+rule calculate_average_speech_length:
+        "Rscript scripts/calculate_speech_length.R {input} {output}"
-------------------------------------------------------------------------
28., repo: kelly-sovacool/asm-microbe-2022, _id: 63116f370ff667b0c9040911:
-rule render:
-        pdf="submission/abstract.pdf"
-        Rscript -e "library(rmarkdown); rmarkdown::render('{input.rmd}', output_file='{output.pdf}')"
-        """
+rule render_pdf:
+        code="code/render.R",
+        preamble="submission/preamble.tex",
+        pdf="submission/abstract.pdf",
+        md="submission/README.md"
+        Rscript {input.code}
+        rm submission/README.html
+        """
-------------------------------------------------------------------------
