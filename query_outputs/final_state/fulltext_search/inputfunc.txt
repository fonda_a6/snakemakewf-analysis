-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: PyPSA/pypsa-za
file: Snakefile
line: ('    input: input_make_summary', 'def input_make_summary(w):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: NBISweden/nbis-meta
file: workflow/Snakefile
line: ('    input: all_input', 'def all_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: dence/rnastar_deseq2_isoseq_trial
file: Snakefile
line: ('\\tinput: all_input', 'def all_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: jmeppley/np_read_clustering
file: rules/Snakefile.polish
line: ('    input: comparison_inputs', 'def comparison_inputs(wildcards, as_dict=False):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: jmeppley/np_read_clustering
file: rules/Snakefile.minimap
line: ('    input: get_cluster_mcl_files', 'def get_cluster_mcl_files(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: elimoss/lathe
file: Snakefile
line: ('\\tinput: choose_assembler()', 'def choose_assembler():')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: elimoss/lathe
file: Snakefile
line: ('\\tinput: choose_contig_cutoff', 'def choose_contig_cutoff(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: elimoss/lathe
file: Snakefile
line: ('\\tinput: aggregate_medaka_subsetruns', 'def aggregate_medaka_subsetruns(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: elimoss/lathe
file: Snakefile
line: ('\\tinput: choose_polish', 'def choose_polish(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: ebi-gene-expression-group/wf-bulk-indexing
file: Snakefile
line: ('    input: aggregate_accessions_update_experiment', 'def aggregate_accessions_update_experiment(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: ebi-gene-expression-group/wf-bulk-indexing
file: Snakefile
line: ('    input: aggregate_accessions_sync_experiment_designs', 'def aggregate_accessions_sync_experiment_designs(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: ebi-gene-expression-group/wf-bulk-indexing
file: Snakefile
line: ('    input: aggregate_baseline_accessions_update_coexpression', 'def aggregate_baseline_accessions_update_coexpression(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: ebi-gene-expression-group/wf-bulk-indexing
file: Snakefile
line: ('    input: aggregate_accessions_load_bulk_analytics_index', 'def aggregate_accessions_load_bulk_analytics_index(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: SkanderHatira/biseps_workflow
file: workflow/comparison/Snakefile
line: ('\\tinput: all_input', 'def all_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: arabidopsisca/snakemake-rna-seq-kallisto-sleuth
file: workflow/Snakefile
line: ('    input: all_input', 'def all_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: crazyhottommy/Genrich_compare
file: Snakefile
line: ('        input: get_input_files', 'def get_input_files(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: alexpenson/rna-seq-kallisto-sleuth
file: workflow/Snakefile
line: ('    input: all_input', 'def all_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: frankier/vocabaqdata
file: workflow/Snakefile
line: ('    input: all_available_input', 'def all_available_input(wc):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: frankier/vocabaqdata
file: workflow/Snakefile
line: ('    input: all_input', 'def all_input(wc):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: felixleopoldo/benchpress
file: workflow/Snakefile
line: ('    input: get_active_rules', 'def get_active_rules(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: pughlab/cfMeDIP-seq-analysis-pipeline
file: Snakefile
line: ('        read_group = lambda wildcards, input: get_read_group_from_fastq(', 'def get_read_group_from_fastq(fastq_file, sample_name):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: dence/rnastar_dseq2_old_RNAseq
file: Snakefile
line: ('\\tinput: all_input', 'def all_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: dence/rnaseq_star_iseq_trial
file: Snakefile
line: ('\\tinput: all_input', 'def all_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: vgteam/vg_snakemake
file: Snakefile
line: ('        input: aggregate_reads', '    def aggregate_reads(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: Monia234/admixMap
file: workflow/Snakefile
line: ('    input: get_all_inputs', 'def get_all_inputs(wildcards): #Primary rule whose input values determine which outputs (and corresponding rules) will be run by snakemake')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: Monia234/admixMap
file: workflow/Snakefile
line: ('    input: get_fine_map_input', 'def get_fine_map_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: ebi-gene-expression-group/bulk-recalculations
file: Snakefile
line: ('    input: input_percentile_ranks', 'def input_percentile_ranks(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: ebi-gene-expression-group/bulk-recalculations
file: Snakefile
line: ('    input: input_differential_tracks_and_gsea', 'def input_differential_tracks_and_gsea(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: ebi-gene-expression-group/bulk-recalculations
file: Snakefile
line: ('    input: input_differential_tracks_and_gsea', 'def input_differential_tracks_and_gsea(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: ebi-gene-expression-group/bulk-recalculations
file: Snakefile
line: ('    input: get_checkpoints_cp_atlas_exps', 'def get_checkpoints_cp_atlas_exps(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: pachterlab/PROBer_paper_analysis
file: extra/GrannemanLab/exp/Snakefile
line: ('\\tinput: get_score_input', 'def get_score_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: pachterlab/PROBer_paper_analysis
file: extra/GrannemanLab/exp/Snakefile
line: ('\\tinput: get_score_input', 'def get_score_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: pachterlab/PROBer_paper_analysis
file: extra/GrannemanLab/exp/Snakefile
line: ('\\tinput: get_score_input', 'def get_score_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: yoshihikosuzuki/purge_tips_smk
file: workflow/Snakefile
line: ('    input: is_converged', 'def is_converged(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: bnelsj/bwa_mem_mapping
file: Snakefile
line: ('    input: lanes_from_sample', 'def lanes_from_sample(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: PyPSA/pypsa-eur-mga
file: Snakefile
line: ('    input: input_generate_all_alternatives', 'def input_generate_all_alternatives(w):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: PyPSA/pypsa-eur-mga
file: Snakefile
line: ('    input: input_generate_clusters_alternatives', 'def input_generate_clusters_alternatives(w):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: crazyhottommy/pyflow-cellranger
file: Snakefile
line: ('\\tinput: get_run_id ', 'def get_run_id(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: crazyhottommy/pyflow-cellranger
file: Snakefile
line: ('\\tinput: get_count_input', 'def get_count_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: crazyhottommy/pyflow-ChIPseq
file: Snakefile
line: ('    input: get_fastq', 'def get_fastq(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: kircherlab/hemoMIPs
file: Snakefile
line: ('  input: getLaneBAMs', 'def getLaneBAMs(wc):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: crazyhottommy/pyflow-scATACseq
file: Snakefile
line: ('    input: get_merge_bam_input', 'def get_merge_bam_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: crazyhottommy/pyflow-scATACseq
file: Snakefile
line: ('    input: get_extend_summit', 'def get_extend_summit(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: dib-lab/2021-metapangenome-example
file: Snakefile
line: ('    input: checkpoint_metabat_2', 'def checkpoint_metabat_2(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: dib-lab/2021-metapangenome-example
file: Snakefile
line: ('    input: checkpoint_charcoal_1', 'def checkpoint_charcoal_1(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: TBradley27/FilTar
file: modules/quant_reads/kallisto/Snakefile
line: ('\\tinput: get_cDNA_file', 'def get_cDNA_file(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: TBradley27/FilTar
file: modules/quant_reads/salmon/Snakefile
line: ('        input:  get_salmon_sample_directory_names', 'def get_salmon_sample_directory_names(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: geparada/rna-seq-kallisto-sleuth
file: workflow/Snakefile
line: ('    input: all_input', 'def all_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: robinmeyers/atac-encode-snakemake
file: Snakefile
line: ('    input: get_target_files', 'def get_target_files(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: open2c/inspectro
file: Snakefile
line: ('    input: generate_targets', 'def generate_targets(wc):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: Monia234/admix-map
file: workflow/Snakefile
line: ('    input: get_all_inputs', 'def get_all_inputs(wildcards): #Primary rule whose input values determine which outputs (and corresponding rules) will be run by snakemake')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: Monia234/admix-map
file: workflow/Snakefile
line: ('    input: get_fine_map_input', 'def get_fine_map_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: taylorreiter/2022-infant-mge
file: Snakefile
line: ('    input: checkpoint_spacegraphcats_extract_mge_sequences_1', 'def checkpoint_spacegraphcats_extract_mge_sequences_1(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: taylorreiter/2022-infant-mge
file: Snakefile
line: ('    input: checkpoint_spacegraphcats_extract_mge_sequences_2', 'def checkpoint_spacegraphcats_extract_mge_sequences_2(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: qbicsoftware-archive/exomseq
file: Snakefile
line: ('    input: fastq_per_group', 'def fastq_per_group(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: qbicsoftware-archive/exomseq
file: Snakefile
line: ('    input: fastq_per_group', 'def fastq_per_group(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: bhattlab/lathe
file: Snakefile
line: ('    input: choose_assembler()', 'def choose_assembler():')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: bhattlab/lathe
file: Snakefile
line: ('    input: choose_contig_cutoff', 'def choose_contig_cutoff(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: bhattlab/lathe
file: Snakefile
line: ('    input: aggregate_medaka_subsetruns', 'def aggregate_medaka_subsetruns(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: bhattlab/lathe
file: Snakefile
line: ('    input: choose_polish', 'def choose_polish(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: gitbackspacer/rna-seq-kallisto-sleuth-test
file: workflow/Snakefile
line: ('    input: all_input', 'def all_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: LUMC/KeyGenes-dataprocessor
file: Snakefile
line: ('    input:gene_inputs', 'def gene_inputs(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: akhanf/tract-fconn-smk
file: workflow/Snakefile
line: ('    input: get_tckfc_maps', 'def get_tckfc_maps(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: AlthafSinghawansaUHN/cfmedipseq_pipeline
file: Snakefile
line: ('        read_group = lambda wildcards, input: get_read_group_from_fastq(', 'def get_read_group_from_fastq(fastq_file, sample_name):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: AlthafSinghawansaUHN/cfmedipseq_pipeline
file: Snakefile
line: ('       read_group = lambda wildcards, input: get_read_group_from_fastq(', 'def get_read_group_from_fastq(fastq_file, sample_name):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: LittleFool/snakemake-workflow-gsea
file: rules/gsea.smk
line: ('\\tinput: gsea_get_folder', 'def gsea_get_folder(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: pburnham50/KallistoPipeline
file: workflow/Snakefile
line: ('    input: all_input', 'def all_input(wildcards):')
-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  
repo: dence/rnastar_deseq2_MeJA_timecourse
file: Snakefile
line: ('\\tinput: all_input', 'def all_input(wildcards):')
