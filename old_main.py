from db_operations.legacy.store_mongo import get_full_coll
from db_operations.commit_queries.filter_commit_types import get_one_parameter_commits
from unidiff import PatchSet

import tempfile
import urllib.request
import json


def display_mongodb_test_data():
    data = get_full_coll("test_data")
    for commit in data:
        print(commit["repo"])
        for file in commit["commit_content"]["files"]:
            if file["changes"] <= 2 and "snakefile" in file["filename"].lower():
                print("FILE: "+file["filename"])
                for key in file.keys():
                    print("    key = "+str(key)+", value = "+str(file[key])[:100])


def parse_patch_from_mongodb():
    data = get_full_coll("test_data")
    data = data[0]
    file = data["commit_content"]["files"][0]
    patch = file["patch"]

    tmp = tempfile.TemporaryFile("w+b")
    tmp.write(patch.encode("utf-8"))
    tmp.seek(0)
    patch = PatchSet(tmp, encoding="utf-8")
    tmp.close()
    for x in patch:
        print(x.path)


def github_api_search(repo):
    search_url = "https://api.github.com/search/code?q=rule+repo:"+repo+"+filename:Snakefile"
    req = urllib.request.Request(search_url)
    req.add_header('Accept', 'application/vnd.github.v3+json')
    req.add_header('Authorization', 'token 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')
    search_result = urllib.request.urlopen(req)
    search_result = json.load(search_result)
    return search_result


def test_api_search():
    data = github_api_search("SichongP/atac-seq")
    if data["total_count"] > 0:
        for result in data["items"]:
            if result["name"] == "Snakefile":
                print("success!")
                print(result["path"])
    print(data.keys())


#url = "https://api.github.com/rate_limit"
#req = urllib.request.Request(url)
#req.add_header('Accept', 'application/vnd.github.v3+json')
#req.add_header('Authorization', 'token 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')
#content = urllib.request.urlopen(req)
#json_content = json.load(content)

#print(content)
#print(json_content)

get_one_parameter_commits()

# data = get_full_coll("data_v1")
# for x in data:
 #    print(x)

#insert_coll("dummy", {"hi": "08.08.2022-13:49"})
#show_coll("dummy")
#dbs = db_op()
#for x in dbs:
#    print(x)
#clear_coll("test_data")


#data = test_db()
#print(data[0])
# data = base64.b64decode(data["file"])
#patch = PatchSet(data["file"], encoding="utf-8")
#for x in patch:
#    print(x.path)
